package pereira.agnaldo.cmccryptocurrency.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;

import java.util.Timer;
import java.util.TimerTask;

public abstract class BaseService extends Service {

    public static String FORCERUN = "forceRun";
    private ServiceHandler mServiceHandler;
    private Timer myTimer;
    private long period = 1000 * 60 * 10;
    private boolean backgroundRunning = false;
    private long delay = 0;

    public void setPeriod(final long period) {
        this.period = period;
    }

    public void setDelay(final long delay) {
        this.delay = delay;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();
        final Looper mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, final int startId) {
        if (myTimer == null) {
            myTimer = new Timer();
            myTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    if (!backgroundRunning) {
                        sendMessage(startId);
                    }
                }
            }, delay, period);
        }

        if (intent != null && intent.hasExtra(FORCERUN) && intent.getBooleanExtra(FORCERUN, false)) {
            sendMessage(startId);
        }
        return START_STICKY;
    }

    private void sendMessage(int startId) {
        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        mServiceHandler.sendMessage(msg);
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public void onDestroy() {
        if (myTimer != null) {
            myTimer.cancel();
            myTimer = null;
        }
        mServiceHandler = null;
        super.onDestroy();
    }

    private final class ServiceHandler extends Handler {
        ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            backgroundRunning = true;
            doInBackground();
            backgroundRunning = false;
        }

    }

    public abstract void doInBackground();
}
