package pereira.agnaldo.cmccryptocurrency.adapter.item;

import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.annimon.stream.Optional;
import com.annimon.stream.Stream;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import org.apache.commons.collections4.CollectionUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem;
import lombok.Getter;
import pereira.agnaldo.cmccryptocurrency.R;
import pereira.agnaldo.cmccryptocurrency.adapter.item.viewholder.CryptoCurrencyViewHolder;
import pereira.agnaldo.cmccryptocurrency.database.model.CryptoCurrency;
import pereira.agnaldo.cmccryptocurrency.database.model.CurrencyHistoric;
import pereira.agnaldo.cmccryptocurrency.network.HttpUrlEndpoint;
import pereira.agnaldo.cmccryptocurrency.network.WebApiBase;
import pereira.agnaldo.cmccryptocurrency.util.DateUtils;
import pereira.agnaldo.cmccryptocurrency.util.Util;

@SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
public class CryptoCurrencyAdapterItem extends AbstractFlexibleItem<CryptoCurrencyViewHolder>
        implements Serializable {

    @Getter
    private CryptoCurrency cryptoCurrency;
    private Context context;
    private CurrencyHistoric currencyHistoric;

    @Override
    public int getLayoutRes() {
        return R.layout.item_crypto_currency;
    }

    public CryptoCurrencyAdapterItem(final Context context, final CryptoCurrency CryptoCurrency) {
        this.cryptoCurrency = CryptoCurrency;
        this.context = context;
    }

    @Override
    public boolean equals(Object o) {
        return this.cryptoCurrency.equals(o);
    }

    @Override
    public CryptoCurrencyViewHolder createViewHolder(final FlexibleAdapter adapter,
                                                     final LayoutInflater
                                                             inflater, final ViewGroup parent) {
        return new CryptoCurrencyViewHolder(inflater.inflate(getLayoutRes(), parent, false),
                adapter);
    }

    @Override
    public void bindViewHolder(final FlexibleAdapter adapter, final CryptoCurrencyViewHolder
            holder, final int position, final List payloads) {
        holder.rank.setText(String.valueOf(cryptoCurrency.getRank()));
        holder.name.setText(cryptoCurrency.getName());
        holder.symbol.setText(cryptoCurrency.getSymbol());
        holder.change_24h.setText(Util.getPercentFormatted(cryptoCurrency.getPercentChange24h()));
        holder.price_usd.setText(Util.getAmountFormatted(cryptoCurrency.getPriceUsd()));
        holder.volume_24h.setText(Util.getAmountFormatted(cryptoCurrency.getVolumeUsd24h()));
        holder.available_supply
                .setText(Util.getAmountFormatted(cryptoCurrency.getAvailableSupply()));
        holder.total_supply.setText(Util.getAmountFormatted(cryptoCurrency.getTotalSupply()));
        holder.market_cap_usd.setText(Util.getAmountFormatted(cryptoCurrency.getMarketCapUsd()));
        cryptoCurrency.loadImageCoin(context, holder.image);

        holder.chart.setDescription(null);
        holder.chart.setNoDataText("historical data is empty, checking for updates ...");

        prepareChartData(holder.chart);
        printChart(holder.chart);
        if (currencyHistoric == null) {
            loadHistoryForOneDay(context, cryptoCurrency, holder.chart);
        }
    }

    private void loadHistoryForOneDay(final Context context,
                                      final CryptoCurrency cryptoCurrency, final LineChart chart) {
        new AsyncTask<Void, Void, CurrencyHistoric>() {

            @Override
            protected CurrencyHistoric doInBackground(final Void... voids) {
                try {
                    final String dateStrartStr = String.valueOf(
                            DateUtils.addDays(cryptoCurrency.getLastUpdated(), -1).getTime());
                    final String dateEndStr =
                            String.valueOf(cryptoCurrency.getLastUpdated().getTime());
                    final WebApiBase<CurrencyHistoric> apiBase = new WebApiBase<>(context);
                    final String endPoint =
                            String.format(HttpUrlEndpoint.GET_CRYPTO_CURRENCY_HISTORY,
                                    cryptoCurrency.getId(), dateStrartStr, dateEndStr);
                    currencyHistoric = apiBase.get(CurrencyHistoric.class, endPoint);
                    prepareChartData(chart);
                    return currencyHistoric;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(final CurrencyHistoric currencyHistoric) {
                super.onPostExecute(currencyHistoric);
                printChart(chart);
            }
        }.execute();
    }

    private void prepareChartData(final LineChart chart) {
        if (currencyHistoric != null && !CollectionUtils.sizeIsEmpty(currencyHistoric.getPrice_usd())) {
            final ArrayList<Entry> yVals = new ArrayList<>();
            for (int i = 0; i < currencyHistoric.getPrice_usd().length; i++) {
                final BigDecimal bigDecimal = currencyHistoric.getPrice_usd()[i][1];
                if (bigDecimal != null) {
                    yVals.add(new Entry(i, bigDecimal.floatValue()));
                }
            }


            LineDataSet set1 = new LineDataSet(yVals, "DataSet 1");
            set1.setAxisDependency(YAxis.AxisDependency.LEFT);

            // set the line to be drawn like this "- - - - - -"
            set1.enableDashedLine(10f, 5f, 0f);
            set1.setColor(Color.BLACK);
            set1.setLineWidth(1f);
            set1.setDrawCircles(false);
            set1.setDrawValues(false);
            set1.setFillAlpha(40);
            set1.setFillColor(Color.LTGRAY);
            set1.setDrawFilled(true);
            LineData data = new LineData(set1);
            chart.setData(data);
        } else {
            chart.setData(null);
        }
    }

    private void printChart(final LineChart chart) {
        final YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setDrawLabels(false);
        leftAxis.setDrawGridLines(false);
        if (currencyHistoric != null && !CollectionUtils.sizeIsEmpty(currencyHistoric.getPrice_usd())) {
            leftAxis.setAxisMaximum(getMax(currencyHistoric.getPrice_usd()));
            leftAxis.setAxisMinimum(getMin(currencyHistoric.getPrice_usd()));
        }

        final XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawLabels(false);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(true);

        chart.getAxisRight().setDrawLabels(false);
        chart.getLegend().setEnabled(false);
        chart.setViewPortOffsets(0, 0, 0, 0);

        chart.setDragEnabled(false);
        chart.setScaleEnabled(false);
        chart.setPinchZoom(false);
        chart.setDoubleTapToZoomEnabled(false);
        chart.setTouchEnabled(false);


        chart.invalidate();
    }

    public float getMax(BigDecimal[][] bigDecimals) {
        if (!CollectionUtils.sizeIsEmpty(bigDecimals)) {
            Optional<BigDecimal> max = Stream.of(bigDecimals)
                    .map(value -> value[1])
                    .max(BigDecimal::compareTo);
            return max.isPresent() ? max.get().floatValue() : 0F;
        }
        return 0F;
    }

    public float getMin(BigDecimal[][] bigDecimals) {
        if (!CollectionUtils.sizeIsEmpty(bigDecimals)) {
            Optional<BigDecimal> max = Stream.of(bigDecimals)
                    .map(value -> value[1])
                    .min(BigDecimal::compareTo);
            return max.isPresent() ? max.get().floatValue() : 0F;
        }
        return 0F;
    }


}
