package pereira.agnaldo.cmccryptocurrency.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

import pereira.agnaldo.cmccryptocurrency.network.NetworkUtils;

@SuppressLint("DefaultLocale")
public class Files {

    private static String getDirBase(final Context context) {
        final String dir;
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            dir = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator
                    + ".CMCCryptoCurrency" + File.separator;
        } else {
            dir = context.getFilesDir().getAbsolutePath() + File.separator + ".CMCCryptoCurrency"
                    + File.separator;
        }

        final File file = new File(dir);
        if (!file.exists()) {
            file.mkdirs();
            try {
                final String command = "chmod 666 " + file.getAbsolutePath();
                Runtime.getRuntime().exec(command);
            } catch (final IOException e) {
                e.printStackTrace();
            }
        }

        return dir;
    }

    public static String getDirBaseCoin(final Context context) {
        return getDirBase(context).concat("Coin").concat(File.separator);
    }

    public static File download(final Context ctx, final String urlFile, final String filePath,
                                final ProgressBar progressBar) {
        try {
            if (NetworkUtils.isConnected(ctx)) {
                final URL url = new URL(urlFile);
                final HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setConnectTimeout(5000);
                urlConnection.setRequestMethod("GET");
                urlConnection.setDoOutput(false);
                urlConnection.connect();

                final File file = new File(filePath.trim());
                try {
                    if (file.exists() && file.isFile()) {
                        file.delete();
                    }
                    file.getParentFile().mkdirs();
                } catch (final Exception e) {
                    e.printStackTrace();
                }

                final InputStream is = urlConnection.getInputStream();

                final FileOutputStream fos = new FileOutputStream(file);

                long total = 0;
                final int lenghtOfFile = urlConnection.getContentLength();
                int bytesRead = -1;
                byte[] buffer = new byte[50];
                while ((bytesRead = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, bytesRead);
                    if (progressBar != null) {
                        try {
                            total += bytesRead;
                            final int percent = (int) ((total * 100) / lenghtOfFile);
                            progressBar.setIndeterminate(false);
                            progressBar.setProgress(percent);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                fos.flush();
                fos.close();
                is.close();

                return file;
            } else {
                return null;
            }

        } catch (final Exception e) {
            e.printStackTrace();
            //            Crashlytics.logException(e);
            return null;
        }
    }

    public static Bitmap download(final Context ctx, final String urlFile, final int maxWidth,
                                  final int maxHeight, final boolean thumbnail) {
        try {
            if (NetworkUtils.isConnected(ctx)) {
                final URL url = new URL(urlFile);
                final HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setConnectTimeout(5000);
                urlConnection.setRequestMethod("GET");
                urlConnection.setDoOutput(false);
                urlConnection.connect();

                final InputStream inputStream = urlConnection.getInputStream();
                final Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                inputStream.close();

                return thumbnail ? ThumbnailUtils.extractThumbnail(
                        bitmap, maxWidth, maxHeight) : bitmap;
            } else {
                return null;
            }

        } catch (final Exception e) {
            e.printStackTrace();
            //Crashlytics.logException(e);
            return null;
        }
    }

    public static void copyFile(final File source, final File destination) throws IOException {
        if (destination.exists()) {
            destination.delete();
        }

        FileChannel sourceChannel = null;
        FileChannel destinationChannel = null;

        try {
            sourceChannel = new FileInputStream(source).getChannel();
            destinationChannel = new FileOutputStream(destination).getChannel();
            sourceChannel.transferTo(0, sourceChannel.size(), destinationChannel);
        } finally {
            if (sourceChannel != null && sourceChannel.isOpen()) {
                sourceChannel.close();
            }
            if (destinationChannel != null && destinationChannel.isOpen()) {
                destinationChannel.close();
            }
        }
    }

    public static void createNoMediaOnDirBase(final Context ctx) {
        try {
            final List<String> dirs = new ArrayList<>();
            dirs.add(getDirBase(ctx));
            dirs.add(getDirBaseCoin(ctx));

            if (!Util.isNullOrEmpty(dirs)) {
                for (final String dir : dirs) {
                    final File file = new File(dir.concat(".nomedia"));
                    if (!file.exists()) {
                        file.getParentFile().mkdirs();
                        file.createNewFile();
                    }
                }
            }
        } catch (final Exception e) {
            e.printStackTrace();
            //Crashlytics.logException(e);
        }
    }

    public static String getNameFileFromURL(final String url) {
        try {
            String name = null;
            if (!TextUtils.isEmpty(url)) {
                String decodedUrl = Uri.decode(url);
                if (decodedUrl.contains("?")) {
                    name = decodedUrl
                            .substring(decodedUrl.lastIndexOf("/") + 1, decodedUrl.indexOf("?"));
                } else {
                    name = decodedUrl.substring(decodedUrl.lastIndexOf("/") + 1);
                }
            }
            return name;
        } catch (final Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void downloadImageAndShow(final Context context, final ImageView imageView,
                                            final String photoUrl, final String filePath, final boolean thumbnail) {
        if (imageView != null) {
            downloadImageAndShow(context, imageView, photoUrl, filePath, thumbnail, null);
        }
    }

    public static void downloadImageAndShow(final Context context, final ImageView imageView,
                                            final String photoUrl, final String filePath,
                                            final boolean thumbnail, final OnTaskFinish onTaskFinish) {
        try {
            if (imageView == null) return;
            if (imageView.getHeight() == 0 && imageView.getWidth() == 0) {
                imageView.post(() -> downloadImageAndShow(context, imageView, photoUrl,
                        filePath, thumbnail, onTaskFinish));
                return;
            }

            final WeakReference<ImageView> viewReference = new WeakReference<>(imageView);
            new AsyncTask<Void, Void, Bitmap>() {
                private int maxWidth, maxHeight;

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    maxWidth = imageView.getWidth();
                    maxHeight = imageView.getHeight();
                }

                @Override
                protected Bitmap doInBackground(final Void... params) {
                    if (Util.isWriteExternalPermited(context)) {
                        File file = new File(filePath);
                        if (!file.exists()) {
                            file = download(context, photoUrl, filePath, null);
                        }

                        if (file != null && file.exists()) {
                            return Files.decodeSampledBitmapFromResource(file.getAbsolutePath(),
                                    maxWidth, maxHeight, thumbnail);
                        }
                    } else {
                        return download(context, photoUrl, maxWidth, maxHeight, thumbnail);
                    }

                    return null;
                }

                @Override
                protected void onPostExecute(final Bitmap bitmap) {
                    super.onPostExecute(bitmap);
                    if (bitmap != null && viewReference.get() != null) {
                        viewReference.get().setImageBitmap(bitmap);
                    }
                    if (onTaskFinish != null) {
                        onTaskFinish.onTaskFinsh();
                    }
                }

            }.execute();

        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    public static Bitmap scaleBitmap(final Bitmap mBitmap, final int newWidth) {
        final int width = mBitmap.getWidth();
        final int height = mBitmap.getHeight();
        if (width > 0 && height > 0) {
            final float scale = ((float) newWidth) / width;
            final Matrix matrix = new Matrix();
            matrix.postScale(scale, scale);
            final Bitmap resizedBitmap = Bitmap.createBitmap(
                    mBitmap, 0, 0, width, height, matrix, false);
            mBitmap.recycle();
            return resizedBitmap;
        }
        return mBitmap;
    }

    public static Bitmap getCroppedBitmap(final String filePath, final int reqWidth,
                                          final int reqHeight, final boolean thumbnail) {
        Bitmap bitmap = decodeSampledBitmapFromResource(filePath, reqWidth, reqHeight, thumbnail);
        if (bitmap != null) {
            bitmap = scaleBitmap(bitmap, reqWidth);
            if (bitmap.getWidth() >= bitmap.getHeight()) {
                final int xStart = bitmap.getWidth() / 2 - reqHeight / 2;
                final int width = (xStart + reqWidth < bitmap.getWidth()) ? reqWidth : reqHeight;
                final int height = reqHeight < bitmap.getHeight() ? reqHeight : bitmap.getHeight();
                return Bitmap.createBitmap(bitmap, xStart, 0, width, height);
            } else {
                int yStart = bitmap.getHeight() / 2 - reqWidth / 2;
                final int width = reqWidth < bitmap.getWidth() ? reqWidth : bitmap.getWidth();
                final int height = (yStart + reqHeight < bitmap.getHeight()) ? reqHeight : reqWidth;
                return Bitmap.createBitmap(bitmap, 0, yStart, width, height);
            }
        }
        return null;
    }

    public static Bitmap decodeSampledBitmapFromResource(
            final String filePath, final int reqWidth, final int reqHeight,
            final boolean thumbnail) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        final Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);
        return thumbnail ? ThumbnailUtils.extractThumbnail(bitmap, reqWidth, reqHeight) : bitmap;
    }

    public static int calculateInSampleSize(
            final BitmapFactory.Options options, final int reqWidth, final int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    public static boolean dumbDB(final Context context) {
        try {
            final File dbDestination = new File(Files.getDirBase(context), "db.sqlite");
            final File dbSource = new File("/data/data/" + context.getPackageName() +
                    "/databases/CMCCryptoCurrency.sqlite");
            Files.copyFile(dbSource, dbDestination);
            return dbDestination.exists();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
