package pereira.agnaldo.cmccryptocurrency.network;

public class HttpUrlEndpoint {

    public static final String GET_ALL_CRYPTO_CURRENCY = "https://api.coinmarketcap.com/v1/ticker/";

    //idBitCoin, millisStart, millisEnd
    public static final String GET_CRYPTO_CURRENCY_HISTORY= "https://graphs.coinmarketcap.com/currencies/%s/%s/%s";


}
