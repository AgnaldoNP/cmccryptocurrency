package pereira.agnaldo.cmccryptocurrency.adapter.item.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;

import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.viewholders.FlexibleViewHolder;
import pereira.agnaldo.cmccryptocurrency.R;

public class CryptoCurrencyViewHolder extends FlexibleViewHolder {

    public TextView rank;
    public TextView name;
    public TextView symbol;
    public TextView change_24h;
    public TextView price_usd;
    public TextView volume_24h;
    public TextView available_supply;
    public TextView total_supply;
    public TextView market_cap_usd;
    public ImageView image;
    public LineChart chart;

    public CryptoCurrencyViewHolder(View view, final FlexibleAdapter adapter) {
        super(view, adapter);
        rank = (TextView) view.findViewById(R.id.rank);
        name = (TextView) view.findViewById(R.id.name);
        symbol = (TextView) view.findViewById(R.id.symbol);
        change_24h = (TextView) view.findViewById(R.id.change_24h);
        price_usd = (TextView) view.findViewById(R.id.price_usd);
        volume_24h = (TextView) view.findViewById(R.id.volume_24h);
        available_supply = (TextView) view.findViewById(R.id.available_supply);
        total_supply = (TextView) view.findViewById(R.id.total_supply);
        market_cap_usd = (TextView) view.findViewById(R.id.market_cap_usd);
        image = (ImageView) view.findViewById(R.id.image);
        chart = (LineChart) view.findViewById(R.id.chart);
    }

}
