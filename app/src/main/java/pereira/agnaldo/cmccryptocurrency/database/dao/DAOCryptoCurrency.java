package pereira.agnaldo.cmccryptocurrency.database.dao;

import com.annimon.stream.Stream;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

import org.apache.commons.collections4.CollectionUtils;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import pereira.agnaldo.cmccryptocurrency.database.DatabaseManager;
import pereira.agnaldo.cmccryptocurrency.database.model.CryptoCurrency;

@SuppressWarnings({"unchecked", "WeakerAccess", "ResultOfMethodCallIgnored"})
public class DAOCryptoCurrency extends DatabaseManager {

    public DAOCryptoCurrency() {
        super();
        DatabaseManager.getInstance();
    }

    public static List<CryptoCurrency> getAll() {
        try {
            final QueryBuilder queryBuilder = getHelper().getDaoCryptoCurrency().queryBuilder();
            queryBuilder.orderBy(CryptoCurrency.RANK, true);
            return queryBuilder.query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public static void saveAll(final List<CryptoCurrency> cryptoCurrencies) {
        if (CollectionUtils.isNotEmpty(cryptoCurrencies)) {
            Stream.of(cryptoCurrencies).forEach(DAOCryptoCurrency::save);
        }
    }

    public static Dao.CreateOrUpdateStatus save(final CryptoCurrency cryptoCurrency) {
        try {
            return getHelper().getDaoCryptoCurrency().createOrUpdate(cryptoCurrency);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int delete(final CryptoCurrency cryptoCurrency) {
        try {
            return getHelper().getDaoCryptoCurrency().delete(cryptoCurrency);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static int deleteAll(final List<CryptoCurrency> cryptoCurrencies) {
        final int[] changes = {0};
        if (CollectionUtils.isNotEmpty(cryptoCurrencies)) {
            Stream.of(cryptoCurrencies).forEach(cryptoCurrency -> {
                changes[0] += delete(cryptoCurrency);
            });
        }
        return changes[0];
    }

    public static int deleteAll() {
        try {
            return getHelper().getDaoCryptoCurrency().deleteBuilder().delete();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

}
