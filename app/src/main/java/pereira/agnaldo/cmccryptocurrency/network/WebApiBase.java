package pereira.agnaldo.cmccryptocurrency.network;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import pereira.agnaldo.cmccryptocurrency.util.DateUtils;
import pereira.agnaldo.cmccryptocurrency.util.StringEx;

public class WebApiBase<T> {

    private Context context;
    private int httpRequestedCode;

    protected InputStream stream;
    private String responseData;
    private Map<String, List<String>> headers;
    protected Gson _gson;

    private int connectionTimeOut = 120000;
    private int readTimeOut = 120000;

    public WebApiBase(final Context context) {
        final GsonBuilder gsonb = new GsonBuilder();
        final DateUtils ds = new DateUtils();
        gsonb.registerTypeAdapter(Date.class, ds);
        _gson = gsonb.create();
        this.context = context;
    }

    private HttpURLConnection getHttpURLConnection(final String urlEndPoint)
            throws Exception {
        final URL url = new URL(urlEndPoint);
        if (urlEndPoint.toLowerCase().startsWith("https")) {
            final HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            return conn;
        } else {
            final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            return conn;
        }
    }

    public boolean get(final String urlEndPoint) throws Exception {
        try {
            final HttpURLConnection conn = getHttpURLConnection(urlEndPoint);
            conn.setRequestMethod("GET");
            makeRequest(conn, null);
            return getHttpRequestedCode() == HttpURLConnection.HTTP_OK;
        } catch (final Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public T get(final Type type, final String urlEndPoint) throws Exception {
        try {
            final HttpURLConnection conn = getHttpURLConnection(urlEndPoint);
            conn.setRequestMethod("GET");
            String result = makeRequest(conn, null);
            return _gson.fromJson(result, type);
        } catch (final Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public T post(final Type type, final String urlEndPoint, final String param) throws Exception {
        final HttpURLConnection conn = getHttpURLConnection(urlEndPoint);
        conn.setRequestMethod("POST");
        final String result = makeRequest(conn, param);
        try {
            return _gson.fromJson(result, type);
        } catch (final Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public boolean post(final String urlEndPoint, final String param) throws Exception {
        final HttpURLConnection conn = getHttpURLConnection(urlEndPoint);
        conn.setRequestMethod("POST");
        makeRequest(conn, param);

        return getHttpRequestedCode() == HttpURLConnection.HTTP_OK;
    }

    public T post(final Type type, final String urlEndPoint) throws Exception {
        final HttpURLConnection conn = getHttpURLConnection(urlEndPoint);
        conn.setRequestMethod("POST");
        final String result = makeRequest(conn, null);
        try {
            return _gson.fromJson(result, type);
        } catch (final Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public void post(final String urlEndPoint) throws Exception {
        final HttpURLConnection conn = getHttpURLConnection(urlEndPoint);
        conn.setRequestMethod("POST");
        makeRequest(conn, null);
    }

    protected String makeRequest(final HttpURLConnection request,
                                      final String param)
            throws Exception, OutOfMemoryError {
        try {
            responseData = null;

            request.setConnectTimeout(this.connectionTimeOut);
            request.setReadTimeout(this.readTimeOut);
            request.setDoInput(true);
            request.setUseCaches(false);

            request.setRequestProperty("Accept", "application/json");

            if (!TextUtils.isEmpty(param)) {
                request.setDoOutput(true);
                request.setRequestProperty("Content-type", "application/json");

                final OutputStream os = request.getOutputStream();
                os.write(param.getBytes("UTF-8"));
                os.flush();
            }

            request.connect();

            headers = request.getHeaderFields();
            httpRequestedCode = request.getResponseCode();
            if (httpRequestedCode != HttpURLConnection.HTTP_OK) {
                stream = request.getErrorStream();
                responseData = StringEx.ConvertStreamToString(stream);
                stream.close();
                request.disconnect();
                throw new HttpResponseException(httpRequestedCode, responseData);
            }

            stream = request.getInputStream();
            responseData = StringEx.ConvertStreamToString(stream);
            stream.close();
            request.disconnect();

        } catch (final Exception e) {
            e.printStackTrace();
            throw e;
        }

        return responseData;
    }

    public void setConnectionTimeOut(int connectionTimeOut) {
        this.connectionTimeOut = connectionTimeOut;
    }

    public void setReadTimeOut(int readTimeOut) {
        this.readTimeOut = readTimeOut;
    }

    public String getResponseData() {
        return responseData;
    }

    public int getHttpRequestedCode() {
        return httpRequestedCode;
    }

    public Map<String, List<String>> getHeaders() {
        return headers;
    }
}