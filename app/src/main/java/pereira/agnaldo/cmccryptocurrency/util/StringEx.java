package pereira.agnaldo.cmccryptocurrency.util;

import android.text.TextUtils;
import android.util.Base64;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.zip.GZIPInputStream;

public class StringEx {

	public static String Base64Encode(final String value) {
		return Base64.encodeToString(value.getBytes(), Base64.URL_SAFE|Base64.NO_WRAP);
	}

	public static String ConvertStreamToString(final InputStream is) throws Exception {
		final ByteArrayOutputStream bos = new ByteArrayOutputStream();
		int next = is.read();
		while (next > -1) {
		    bos.write(next);
		    next = is.read();
		}
		bos.flush();
		final byte[] result = bos.toByteArray();

		if(result.length != 0){
			String resultstr;
			if (isGzipStream(result)){
				resultstr = decompress(result);
			}else{
				resultstr = new String(result, "UTF-8");
			}
			return resultstr;
		}else{
			return null;
		}
	}

	public static String decompress(final byte[] bytes) throws Exception {
        if (bytes == null || bytes.length == 0) {
            return null;
        }
        final GZIPInputStream gis = new GZIPInputStream(new ByteArrayInputStream(bytes));
        final BufferedReader bf = new BufferedReader(new InputStreamReader(gis, "UTF-8"));
        String outStr = "";
        String line;
        while ((line=bf.readLine())!=null) {
          outStr += line;
        }
        return outStr;
     }

	public static boolean toBoolean(final String s) {
		return (!TextUtils.isEmpty(s) && s.trim().equals("1"));
	}

	public static boolean isGzipStream(final byte[] bytes) {
	      final int head = (bytes[0] & 0xff) | ((bytes[1] << 8) & 0xff00);
	      return (GZIPInputStream.GZIP_MAGIC == head);
	}

}

