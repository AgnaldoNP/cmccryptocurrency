package pereira.agnaldo.cmccryptocurrency.database;

import android.content.Context;


public class DatabaseManager {
    static private DatabaseManager instance;
    static protected Context context;

    static public void init(Context ctx) {
        if (null == instance) {
            context = ctx;
            instance = new DatabaseManager(ctx);
        }
    }

    static public DatabaseManager getInstance() {
        return instance;
    }

    private static DatabaseHelper helper;

    public DatabaseManager() {
    }

    public DatabaseManager(Context ctx) {
        context = ctx;
        helper = new DatabaseHelper(ctx);
        helper.getWritableDatabase();
    }

    protected static DatabaseHelper getHelper() {
        return helper;
    }
}