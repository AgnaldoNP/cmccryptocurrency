package pereira.agnaldo.cmccryptocurrency.util;

import android.annotation.SuppressLint;
import android.text.TextUtils;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtils implements JsonDeserializer<Object> {

    public static final String FORMAT_DATETIME_DEFAULT_EN = "yyyy-MM-dd HH:mm:ss";
    private static final String FORMAT_DATETIME_DEFAULT_EN_T = "yyyy-MM-dd'T'HH:mm:ss";
    private static final String FORMAT_DATETIME_SLASH_EN = "yyyy/MM/dd HH:mm:ss";
    private static final String FORMAT_DATETIME_SLASH_EN_T = "yyyy/MM/dd'T'HH:mm:ss";

    @Override
    @SuppressLint("UseValueOf")
    public Object deserialize(final JsonElement json, final Type arg1,
            final JsonDeserializationContext arg2) throws JsonParseException {
        return parseStringToDate(json.getAsJsonPrimitive().getAsString());
    }

    public static Date getNow() {
        final Date now = new Date(System.currentTimeMillis());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(now);
        return calendar.getTime();
    }

    public static Date parseStringToDate(final String dateInString){
        try {
            if(!TextUtils.isEmpty(dateInString)){
                if(dateInString.contains("-")){
                    final SimpleDateFormat formatter = new SimpleDateFormat(
                            dateInString.contains("T") ? FORMAT_DATETIME_DEFAULT_EN_T :
                                    FORMAT_DATETIME_DEFAULT_EN, Locale.US);
                    return formatter.parse(dateInString);
                } else if(dateInString.contains("/")){
                    final SimpleDateFormat formatter = new SimpleDateFormat(
                            dateInString.contains("T") ? FORMAT_DATETIME_SLASH_EN_T :
                                    FORMAT_DATETIME_SLASH_EN, Locale.US);
                    return formatter.parse(dateInString);
                } else{
                    final long longDate = Long.parseLong(dateInString);
                    return  new Date(longDate > 9999999999L ? longDate : longDate *1000);
                }
            }else{
                return null;
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String parseDateToString(final Date date) {
        if (date != null) {
            final DateFormat df = new SimpleDateFormat(FORMAT_DATETIME_DEFAULT_EN, Locale.US);
            return df.format(date.getTime());
        } else {
            return null;
        }
    }

    public static Date addDays(final Date date, final int i) {
        if(date != null){
            final Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(date.getTime());
            calendar.add(Calendar.DAY_OF_YEAR, i);
            return calendar.getTime();
        }
        return null;
    }
}