package pereira.agnaldo.cmccryptocurrency.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.google.gson.reflect.TypeToken;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.IgnoreWhen;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.api.BackgroundExecutor;
import org.apache.commons.collections4.CollectionUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.common.SmoothScrollLinearLayoutManager;
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem;
import pereira.agnaldo.cmccryptocurrency.R;
import pereira.agnaldo.cmccryptocurrency.activity.MainActivity;
import pereira.agnaldo.cmccryptocurrency.adapter.CryptoCurrencyListAdapter;
import pereira.agnaldo.cmccryptocurrency.adapter.item.CryptoCurrencyAdapterItem;
import pereira.agnaldo.cmccryptocurrency.database.dao.DAOCryptoCurrency;
import pereira.agnaldo.cmccryptocurrency.database.model.CryptoCurrency;
import pereira.agnaldo.cmccryptocurrency.network.HttpUrlEndpoint;
import pereira.agnaldo.cmccryptocurrency.network.WebApiBase;

@EFragment(R.layout.fragment_crypto_currency_list)
public class CryptoCurrencyListFragment extends Fragment
        implements FlexibleAdapter.OnItemClickListener, FlexibleAdapter.OnItemLongClickListener {

    private final static String LOADCRYPTOCURRENCIESTASKTAG = "loadCryptoCurrencies";
    private final static String KEY_CRYPTOCURRENCIES = "cryptoCurrencies";

    @ViewById(R.id.simple_recycle_view_id)
    RecyclerView recyclerView;

    @ViewById(R.id.simple_recycle_view_progressbar)
    View progress;

    @ViewById(R.id.noData)
    TextView noData;

    private ArrayList<CryptoCurrency> cryptoCurrencies;
    private MainActivity mainActivity;
    private CryptoCurrencyListAdapter listAdapter;

    public CryptoCurrencyListFragment() {
        super();
    }

    @AfterViews
    protected void afterViews() {
        mainActivity = (MainActivity) getActivity();
        mainActivity.setVisible(progress, true);
        mainActivity.setVisible(noData, false);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(KEY_CRYPTOCURRENCIES, cryptoCurrencies);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey(KEY_CRYPTOCURRENCIES)) {
            cryptoCurrencies = savedInstanceState.getParcelableArrayList(KEY_CRYPTOCURRENCIES);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        BackgroundExecutor.cancelAll(LOADCRYPTOCURRENCIESTASKTAG, true);
        if (CollectionUtils.isEmpty(cryptoCurrencies)) {
            loadCryptoCurrencies();
        }else{
            createAdapter();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Background
    @IgnoreWhen(IgnoreWhen.State.VIEW_DESTROYED)
    protected void createAdapter() {
        List<AbstractFlexibleItem> contactsFlexibleItems = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(cryptoCurrencies)) {
            Stream.of(cryptoCurrencies).forEach(cryptoCurrency -> contactsFlexibleItems.add(
                    new CryptoCurrencyAdapterItem(mainActivity, cryptoCurrency)));
        }

        listAdapter = new CryptoCurrencyListAdapter(contactsFlexibleItems, this);
        setAdapterOnRecyclerView();
    }

    @Background
    @IgnoreWhen(IgnoreWhen.State.VIEW_DESTROYED)
    protected void updateAdapter(final List<CryptoCurrency> cryptoCurrenciesUpdated) {
        listAdapter.update(cryptoCurrenciesUpdated);
    }

    @UiThread
    @IgnoreWhen(IgnoreWhen.State.VIEW_DESTROYED)
    protected void setAdapterOnRecyclerView() {
        recyclerView.setLayoutManager(new SmoothScrollLinearLayoutManager(
                mainActivity, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(listAdapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.scrollToPosition(0);
        mainActivity.setVisible(progress, false);

        mainActivity.setVisible(noData, listAdapter.getItemCount() == 0);
    }

    @Override
    public boolean onItemClick(int position) {
        final CryptoCurrencyAdapterItem cryptoCurrencyAdapterItem = getCryptoCurrencyAdapterItemFromPosition(position);
        if (cryptoCurrencyAdapterItem != null && cryptoCurrencyAdapterItem.getCryptoCurrency() != null) {
            //TODO fazer alguma coisa ...
        }
        return false;
    }

    @Override
    public void onItemLongClick(int position) {
        onItemClick(position);
    }

    private CryptoCurrencyAdapterItem getCryptoCurrencyAdapterItemFromPosition(final int position) {
        final AbstractFlexibleItem item = listAdapter.getItem(position);
        if (item != null && item instanceof CryptoCurrencyAdapterItem) {
            return (CryptoCurrencyAdapterItem) item;
        }
        return null;
    }

    @Background(id = LOADCRYPTOCURRENCIESTASKTAG)
    protected void loadCryptoCurrencies() {
        cryptoCurrencies = (ArrayList<CryptoCurrency>) DAOCryptoCurrency.getAll();
        if (CollectionUtils.isNotEmpty(cryptoCurrencies)) {
            sortCryptoCurrencies();
        }
        createAdapter();
        getAllCryptoCurrency();
    }

    private void sortCryptoCurrencies() {
        cryptoCurrencies = (ArrayList<CryptoCurrency>) Stream.of(cryptoCurrencies)
                .sorted((cryptoCurrency1, cryptoCurrency2) -> {
                    final int rank1 = cryptoCurrency1.getRank();
                    final int rank2 = cryptoCurrency2.getRank();
                    return Integer.compare(rank1, rank2);
                }).collect(Collectors.toList());
    }


    @Background
    protected void getAllCryptoCurrency() {
        try {
            final WebApiBase<ArrayList<CryptoCurrency>> apiBase = new WebApiBase<>(mainActivity);
            final Type type = new TypeToken<ArrayList<CryptoCurrency>>() {
            }.getType();
            final String urlEndPoint = HttpUrlEndpoint.GET_ALL_CRYPTO_CURRENCY;
            final ArrayList<CryptoCurrency> cryptoCurrenciesFromWeb = apiBase.get(type, urlEndPoint);
            DAOCryptoCurrency.saveAll(cryptoCurrenciesFromWeb);

            cryptoCurrencies = cryptoCurrenciesFromWeb;
            sortCryptoCurrencies();

            if (listAdapter.getItemCount() == 0) {
                createAdapter();
            } else {
                updateAdapter(cryptoCurrenciesFromWeb);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}