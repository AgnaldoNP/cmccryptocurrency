package pereira.agnaldo.cmccryptocurrency.util;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Collection;
import java.util.Currency;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Set;

@SuppressLint("DefaultLocale")
public class Util {

    public static final String FORMAT_DATETIME_DEFAULT_EN = "yyyy/MM/dd HH:mm:ss";

    public static int getWidthOfScrern(final Context ctx) {
        try {
            final WindowManager wm = (WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE);
            final Display display = wm.getDefaultDisplay();
            return display.getWidth();
        } catch (final Exception e) {
            return 0;
        }
    }

    public static int getHeightOfScrern(final Context ctx) {
        try {
            final WindowManager wm = (WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE);
            final Display display = wm.getDefaultDisplay();
            return display.getHeight();
        } catch (final Exception e) {
            return 0;
        }
    }

    public static String convertFloatToString(final Float f) {
        try {
            return String.format("%.2f", f);
        } catch (final Exception e) {
            return "";
        }
    }

    public static String convertDoubleToString(final Double doubleValue) {
        try {
            return String.format("%.2f", doubleValue).replaceAll("[.]", ",");
        } catch (final Exception e) {
            return "";
        }
    }

    public static boolean isNullOrEmpty(final Collection<?> c) {
        return c == null || c.isEmpty();
    }

    public static boolean isNullOrEmpty(final Object[] objects) {
        return objects == null || objects.length == 0;
    }

    public static boolean isNull(final Object... objects) {
        try {
            if (objects == null) {
                return true;
            }

            for (final Object obj : objects) {
                if (obj != null) {
                    return false;
                }
            }
            return true;
        } catch (final Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static int convertDpToPx(final Context context, final int dp) {
        final DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        final int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    public static int convertPxToDp(final Context context, final int px) {
        final DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        final int dp = Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return dp;
    }

    public static float convertPxToSp(final Context context, final float px) {
        final float scaledDensity = context.getResources().getDisplayMetrics().scaledDensity;
        return px / scaledDensity;
    }

    public static float convertSpToPx(final Context context, final float sp) {
        final float scaledDensity = context.getResources().getDisplayMetrics().scaledDensity;
        return sp * scaledDensity;
    }

    public static <I, T extends Collection<I>> T removeDuplicates(final T collection) {
        final Set<I> setItems = new LinkedHashSet<I>(collection);
        collection.clear();
        collection.addAll(setItems);
        return collection;
    }

    public static boolean isTrue(Boolean aBoolean) {
        return aBoolean != null && aBoolean;
    }

    public static String getRealPathFromURI(final Context context, final String contentURI) {
        final Uri contentUri = Uri.parse(contentURI);
        final Cursor cursor =
                context.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            final int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public static boolean isWriteExternalPermited(final Context context) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            return ContextCompat.checkSelfPermission(context.getApplicationContext(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                    PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(context.getApplicationContext(),
                            Manifest.permission.READ_EXTERNAL_STORAGE) ==
                            PackageManager.PERMISSION_GRANTED;
        } else {
            return ContextCompat.checkSelfPermission(context.getApplicationContext(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                    PackageManager.PERMISSION_GRANTED;
        }
    }

    public static boolean isGyroscopeAvailable(final Context context) {
        SensorManager mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        return mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE) != null;
    }

    public static String getAmountFormatted(final BigDecimal value) {
        if (value != null) {
            final String symbolCurrency = Currency.getInstance(Locale.US).getSymbol();
            return symbolCurrency.concat(" ").concat(NumberFormat.getNumberInstance(Locale.US).format(value));
        }
        return StringUtils.EMPTY;
    }

    public static String getPercentFormatted(final double value) {
        final NumberFormat defaultFormat = NumberFormat.getPercentInstance();
        defaultFormat.setMinimumFractionDigits(2);
        return defaultFormat.format(value/100);
    }
}
