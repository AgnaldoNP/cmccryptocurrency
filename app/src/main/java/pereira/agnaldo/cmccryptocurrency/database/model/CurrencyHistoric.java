package pereira.agnaldo.cmccryptocurrency.database.model;

import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@DatabaseTable(tableName = "CryptoCurrency")
public class CurrencyHistoric implements Serializable {

    private BigDecimal[][] market_cap_by_available_supply;
    private BigDecimal[][] price_btc;
    private BigDecimal[][] price_usd;
    private BigDecimal[][] volume_usd;
//    public List getHistoricalPriceUSD(){
//
//        return Collections.emptyList();
//
//    }
//

}
