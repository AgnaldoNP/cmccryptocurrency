package pereira.agnaldo.cmccryptocurrency.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import pereira.agnaldo.cmccryptocurrency.database.model.CryptoCurrency;


public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
    private static final String DATABASE_NAME = "CMCCryptoCurrency.sqlite";
    private static final int DATABASE_VERSION = 1;
    Context _context;

    private Dao<CryptoCurrency, String> daoCryptoCurrency;

    public DatabaseHelper(final Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        _context = context;
    }

    @Override
    public void onCreate(final SQLiteDatabase database, final ConnectionSource connectionSource) {
        try {
            TableUtils.createTableIfNotExists(connectionSource, CryptoCurrency.class);
        } catch (final Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(final SQLiteDatabase db, final ConnectionSource connectionSource,
                          final int oldVersion, final int newVersion) {
        try {
            if (oldVersion < 1) {
                //updateTo2(db, connectionSource);
            }
            onCreate(db, connectionSource);
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    public Dao<CryptoCurrency, String> getDaoCryptoCurrency() {
        if (daoCryptoCurrency == null) {
            try {
                daoCryptoCurrency = getDao(CryptoCurrency.class);
            } catch (final Exception e) {
                e.printStackTrace();
            }
        }
        return daoCryptoCurrency;
    }

}
