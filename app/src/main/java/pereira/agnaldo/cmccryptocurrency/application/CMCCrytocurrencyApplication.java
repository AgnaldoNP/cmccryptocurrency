package pereira.agnaldo.cmccryptocurrency.application;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import org.androidannotations.annotations.EApplication;

import pereira.agnaldo.cmccryptocurrency.database.DatabaseManager;
import pereira.agnaldo.cmccryptocurrency.util.Files;

@EApplication
public class CMCCrytocurrencyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Files.createNoMediaOnDirBase(this);
        DatabaseManager.init(this);
    }

    @Override
    protected void attachBaseContext(Context contextBase) {
        super.attachBaseContext(contextBase);
        MultiDex.install(contextBase);
    }

}
