package pereira.agnaldo.cmccryptocurrency.util;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class Keyboard {
	public static void close(View v){
		InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
	}

	public static void open(View v){
		final InputMethodManager imm = (InputMethodManager)
				v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
		if (!v.hasFocus()) {
			v.requestFocus();
		}
		v.post(() -> imm.showSoftInput(v, InputMethodManager.SHOW_FORCED));
	}

}
