package pereira.agnaldo.cmccryptocurrency.database.model;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ImageView;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.apache.commons.lang3.builder.EqualsBuilder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import pereira.agnaldo.cmccryptocurrency.R;
import pereira.agnaldo.cmccryptocurrency.util.Files;

@Getter
@Setter
@DatabaseTable(tableName = "CryptoCurrency")
public class CryptoCurrency implements Serializable, Parcelable {

    public static final String ID = "id";
    @DatabaseField(columnName = ID, id = true)
    @SerializedName("id")
    private String id;

    public static final String NAME = "name";
    @DatabaseField(columnName = NAME)
    @SerializedName("name")
    private String name;

    public static final String SYMBOL = "symbol";
    @DatabaseField(columnName = SYMBOL)
    @SerializedName("symbol")
    private String symbol;

    public static final String RANK = "rank";
    @DatabaseField(columnName = RANK)
    @SerializedName("rank")
    private int rank;

    public static final String PRICE_USD = "price_usd";
    @DatabaseField(columnName = PRICE_USD)
    @SerializedName("price_usd")
    private BigDecimal priceUsd;

    public static final String PRICE_BTC = "price_btc";
    @DatabaseField(columnName = PRICE_BTC)
    @SerializedName("price_btc")
    private BigDecimal priceBtc;

    public static final String VOLUME_USD_24H = "volume_usd_24h";
    @DatabaseField(columnName = VOLUME_USD_24H)
    @SerializedName("24h_volume_usd")
    private BigDecimal VolumeUsd24h;

    public static final String MARKET_CAP_USD = "market_cap_usd";
    @DatabaseField(columnName = MARKET_CAP_USD)
    @SerializedName("market_cap_usd")
    private BigDecimal marketCapUsd;

    public static final String AVALIABLE_SUPPLY = "available_supply";
    @DatabaseField(columnName = AVALIABLE_SUPPLY)
    @SerializedName("available_supply")
    private BigDecimal availableSupply;

    public static final String TOTAL_SUPPLY = "total_supply";
    @DatabaseField(columnName = TOTAL_SUPPLY)
    @SerializedName("total_supply")
    private BigDecimal totalSupply;

    public static final String PERCENT_CHANGE_1H = "percent_change_1h";
    @DatabaseField(columnName = PERCENT_CHANGE_1H)
    @SerializedName("percent_change_1h")
    private double percentChange1h;

    public static final String PERCENT_CHANGE_24H = "percent_change_24h";
    @DatabaseField(columnName = PERCENT_CHANGE_24H)
    @SerializedName("percent_change_24h")
    private double percentChange24h;

    public static final String PERCENT_CHANGE_7D = "percent_change_7d";
    @DatabaseField(columnName = PERCENT_CHANGE_7D)
    @SerializedName("percent_change_7d")
    private double percentChange7d;

    public static final String LAST_UPDATED = "last_updated";
    @DatabaseField(columnName = LAST_UPDATED)
    @SerializedName("last_updated")
    private Date lastUpdated;

    public String getUrlImageCoin(){
        return String.format("https://files.coinmarketcap.com/static/img/coins/128x128/%s.png", id);
    }

    public void loadImageCoin(final Context context, final ImageView imageView){
        if(imageView != null){
            imageView.setImageResource(R.mipmap.ic_launcher);
            final String fileName = Files.getNameFileFromURL(getUrlImageCoin());
            final String userAvatarPath = Files.getDirBaseCoin(context) + fileName;
            Files.downloadImageAndShow(context, imageView, getUrlImageCoin(), userAvatarPath, true);
        }
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (! (other instanceof CryptoCurrency)) {
            return false;
        }
        return new EqualsBuilder().append(id, ((CryptoCurrency) other).id).isEquals();
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.symbol);
        dest.writeInt(this.rank);
        dest.writeSerializable(this.priceUsd);
        dest.writeSerializable(this.priceBtc);
        dest.writeSerializable(this.VolumeUsd24h);
        dest.writeSerializable(this.marketCapUsd);
        dest.writeSerializable(this.availableSupply);
        dest.writeSerializable(this.totalSupply);
        dest.writeDouble(this.percentChange1h);
        dest.writeDouble(this.percentChange24h);
        dest.writeDouble(this.percentChange7d);
        dest.writeLong(this.lastUpdated != null ? this.lastUpdated.getTime() : -1);
    }

    public CryptoCurrency() {
    }

    protected CryptoCurrency(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.symbol = in.readString();
        this.rank = in.readInt();
        this.priceUsd = (BigDecimal) in.readSerializable();
        this.priceBtc = (BigDecimal) in.readSerializable();
        this.VolumeUsd24h = (BigDecimal) in.readSerializable();
        this.marketCapUsd = (BigDecimal) in.readSerializable();
        this.availableSupply = (BigDecimal) in.readSerializable();
        this.totalSupply = (BigDecimal) in.readSerializable();
        this.percentChange1h = in.readDouble();
        this.percentChange24h = in.readDouble();
        this.percentChange7d = in.readDouble();
        long tmpLastUpdated = in.readLong();
        this.lastUpdated = tmpLastUpdated == -1 ? null : new Date(tmpLastUpdated);
    }

    public static final Parcelable.Creator<CryptoCurrency> CREATOR = new Parcelable.Creator<CryptoCurrency>() {
        @Override
        public CryptoCurrency createFromParcel(Parcel source) {
            return new CryptoCurrency(source);
        }

        @Override
        public CryptoCurrency[] newArray(int size) {
            return new CryptoCurrency[size];
        }
    };
}
