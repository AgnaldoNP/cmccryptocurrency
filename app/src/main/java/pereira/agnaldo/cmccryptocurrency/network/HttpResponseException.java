package pereira.agnaldo.cmccryptocurrency.network;

public class HttpResponseException extends Exception {
    private int statusCode;

    public HttpResponseException(final int statusCode, final String detailMessage) {
        super(detailMessage);
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }

}
