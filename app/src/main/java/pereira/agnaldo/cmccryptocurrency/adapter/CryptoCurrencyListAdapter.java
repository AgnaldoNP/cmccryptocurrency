package pereira.agnaldo.cmccryptocurrency.adapter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.annimon.stream.Stream;

import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem;
import pereira.agnaldo.cmccryptocurrency.database.model.CryptoCurrency;

public class CryptoCurrencyListAdapter extends FlexibleAdapter<AbstractFlexibleItem> {

    List<AbstractFlexibleItem> allItems;

    public CryptoCurrencyListAdapter(@Nullable List<AbstractFlexibleItem> items, @Nullable Object listeners) {
        super(items, listeners);
        if (items != null) {
            allItems = new ArrayList<>(items);
        }
    }

    @Override
    public boolean addItem(@NonNull AbstractFlexibleItem item) {
        if (allItems == null) {
            allItems = new ArrayList<>();
        }
        if (!allItems.contains(item)) {
            allItems.add(item);
            if (!contains(item)) {
                return super.addItem(item);
            }
        }
        return false;
    }

    public void addItems(List<AbstractFlexibleItem> items) {
        if(CollectionUtils.isNotEmpty(items)){
            Stream.of(items).forEach(this::addItem);
        }
    }

    @Override
    public void onLoadMoreComplete(@Nullable List<AbstractFlexibleItem> newItems) {
        super.onLoadMoreComplete(newItems);
        if (newItems != null) {
            if (allItems == null) {
                allItems = new ArrayList<>();
            }
            allItems.addAll(newItems);
        }
    }

    public void update(List<CryptoCurrency> cryptoCurrenciesUpdated) {
        //Todo fazer replace das cryptomoedas e adicionar caso não tenha
    }
}
